// Orpheus - Guitar
var camera;
var prevImg;
var currImg;    
var diffImg;
var threshold;
var grid;

/*Audio initiate*/
var audioInit = false;

//Color-cover slider
var colorCoverSlider;

/*Chords sounds*/
/*C-chord*/
var CStringA;
var CStringG;
var CStringD;
var CStringB;
var CStringe;
var CChordEnv1;
var CChordEnv2;
var CChordEnv3;
var CChordEnv4;
var CChordEnv5;

/*G-chord*/
var GStringE;
var GStringA;
var GStringD;
var GStringG;
var GStringB;
var GStringe;
var GChordEnv1;
var GChordEnv2;
var GChordEnv3;
var GChordEnv4;
var GChordEnv5;
var GChordEnv6;

/*Am-chord*/
var AmStringA;
var AmStringD;
var AmStringG;
var AmStringB;
var AmStringe;
var AmChordEnv1;
var AmChordEnv2;
var AmChordEnv3;
var AmChordEnv4;
var AmChordEnv5;

/*F-chord*/
var FStringD;
var FStringG;
var FStringB;
var FStringe;
var FChordEnv1;
var FChordEnv2;
var FChordEnv3;
var FChordEnv4;

/*Modulator*/
var stringModulator;

/*Chord text - Booleans to trigger text*/
var C = false;
var G = false;
var Am = false;
var F = false;

function setup() {
    var canvas = createCanvas(windowWidth/2, windowHeight);
    canvas.parent('sketch'); //The sketch now belongs to the html DIV "sketch" and can so be positioned within the DOM
    pixelDensity(1);
    camera = createCapture(VIDEO);
    camera.hide();
    background(255,0);
    
    /*Audio*/
    audioContext = new maximJs.maxiAudio();
    audioContext.play = playLoop;
    
    threshold = 0.070;
    grid = new Grid(640, 480);
    
    //Slider that lets you choose what colour you would like your figure to be tinted with
    colorCoverSlider = createSlider(0,255,0,10);
    colorCoverSlider.position(windowWidth * 0.1,windowHeight * 0.7);
    
    /*C chord strings*/
    CStringA = new maximJs.maxiOsc();
    CStringD = new maximJs.maxiOsc();
    CStringG = new maximJs.maxiOsc();
    CStringB = new maximJs.maxiOsc();
    CStringe = new maximJs.maxiOsc();
    /*Envelope*/
    CChordEnv1 = new maximEx.env();
    CChordEnv2 = new maximEx.env();
    CChordEnv3 = new maximEx.env();
    CChordEnv4 = new maximEx.env();
    CChordEnv5 = new maximEx.env();
    
    /*G chord strings*/
    GStringE = new maximJs.maxiOsc();
    GStringA = new maximJs.maxiOsc();
    GStringD = new maximJs.maxiOsc();
    GStringG = new maximJs.maxiOsc();
    GStringB = new maximJs.maxiOsc();
    GStringe = new maximJs.maxiOsc();
    /*Envelope*/
    GChordEnv1 = new maximEx.env();
    GChordEnv2 = new maximEx.env();
    GChordEnv3 = new maximEx.env();
    GChordEnv4 = new maximEx.env();
    GChordEnv5 = new maximEx.env();
    GChordEnv6 = new maximEx.env();
    
    /*Am chord strings*/
    AmStringA = new maximJs.maxiOsc();
    AmStringD = new maximJs.maxiOsc();
    AmStringG = new maximJs.maxiOsc();
    AmStringB = new maximJs.maxiOsc();
    AmStringe = new maximJs.maxiOsc();
    /*Envelope*/
    AmChordEnv1 = new maximEx.env();
    AmChordEnv2 = new maximEx.env();
    AmChordEnv3 = new maximEx.env();
    AmChordEnv4 = new maximEx.env();
    AmChordEnv5 = new maximEx.env();
    
    /*F chord strings*/
    FStringD = new maximJs.maxiOsc();
    FStringG = new maximJs.maxiOsc();
    FStringB = new maximJs.maxiOsc();
    FStringe = new maximJs.maxiOsc();
    /*Envelope*/
    FChordEnv1 = new maximEx.env();
    FChordEnv2 = new maximEx.env();
    FChordEnv3 = new maximEx.env();
    FChordEnv4 = new maximEx.env();
    
    /*Modulator*/
    stringModulator = new maximJs.maxiOsc();
    
}

function playLoop() {
    
    var sig = 0;
    
    /*C-chord*/
    var cChord = CStringA.triangle(131) * CChordEnv1.ar(0.1,1.5) + CStringD.triangle(164) * CChordEnv2.ar(0.2,1.5) + CStringG.triangle(195) * CChordEnv3.ar(0.3,1.5)+ CStringB.triangle(262) * CChordEnv4.ar(0.4,1.5) + CStringe.triangle(329) * CChordEnv5.ar(0.5,1.5);
    
    /*G-chord*/
    var gChord = GStringE.triangle(98) * GChordEnv1.ar(0.1,1.5) + GStringA.triangle(123) * GChordEnv2.ar(0.2,1.5) + GStringD.triangle(146) * GChordEnv3.ar(0.3,1.5) + GStringG.triangle(195) * GChordEnv4.ar(0.4,1.5) + GStringB.triangle(246) * GChordEnv5.ar(0.5,1.5)+ GStringe.triangle(392) * GChordEnv6.ar(0.6,1.5);
  
    /*Am-chord*/
    var amChord = AmStringA.triangle(109) * AmChordEnv1.ar(0.1,1.5) + AmStringD.triangle(164) * AmChordEnv2.ar(0.2,1.5) + AmStringG.triangle(219) * AmChordEnv3.ar(0.3,1.5) + AmStringB.triangle(262) * AmChordEnv4.ar(0.4,1.5) + AmStringe.triangle(329) * AmChordEnv5.ar(0.5,1.5);
    
    /*F-chord*/
    var fChord = FStringD.triangle(174) * FChordEnv1.ar(0.1,1.5) + FStringG.triangle(219) * FChordEnv2.ar(0.2,1.5) + FStringB.triangle(262) * FChordEnv3.ar(0.3,1.5) + FStringe.triangle(349) * FChordEnv4.ar(0.4,1.5);
    
    /*If chord is triggered, play that specific chord.*/
    if(C == true)
    {
        sig += cChord;
        CChordEnv1.trigger();
        CChordEnv2.trigger();
        CChordEnv3.trigger();
        CChordEnv4.trigger();
        CChordEnv5.trigger();
    }
    
    if(G == true)
    {
        sig += gChord;
        GChordEnv1.trigger();
        GChordEnv2.trigger();
        GChordEnv3.trigger();
        GChordEnv4.trigger();
        GChordEnv5.trigger();
        GChordEnv6.trigger();
    }
    
    if(Am == true)
    {
        sig += amChord;
        AmChordEnv1.trigger();
        AmChordEnv2.trigger();
        AmChordEnv3.trigger();
        AmChordEnv4.trigger();
        AmChordEnv5.trigger();
    }
    
    if(F == true)
    {
        sig = fChord;
        FChordEnv1.trigger();
        FChordEnv2.trigger();
        FChordEnv3.trigger();
        FChordEnv4.trigger();
        
    }


    
    this.output = sig * 0.2;
    
}

function draw() { 

    /*Take camera input and position the output*/
    image(camera, 0, 0);
    camera.loadPixels();

    var smallW = camera.width/4;
    var smallH = camera.height/4;
    currImg = createImage(smallW, smallH);
    currImg.copy(camera, 0, 0, camera.width, camera.height, 0, 0, smallW, smallH); /*Saving a copy of the current frame*/
    currImg.filter(GRAY);
    currImg.filter(BLUR,3);
    diffImg = createImage(smallW,smallH); /*This image will have the threshold filter, the white colour will show up where there was last movement using frame differencing.*/

    if (typeof prevImg !== 'undefined') 
    {
        currImg.loadPixels();
        prevImg.loadPixels();
        diffImg.loadPixels();
        
        /*Looping through all pixels of the currImg and if a pixel is brighter in the current image (there has been movement there), set the equivalent pixel in diffImg to white.*/
        for (var x = 0; x < currImg.width; x += 1) {
            for (var y = 0; y < currImg.height; y += 1) {
                var index = (x + (y * currImg.width)) * 4;
                var redSource = currImg.pixels[index + 0];
                
                var rPrev = prevImg.pixels[index + 0];
                
                var d = Math.abs(redSource - rPrev);
                diffImg.pixels[index + 0] = d;
                diffImg.pixels[index + 1] = d;
                diffImg.pixels[index + 2] = d;
                diffImg.pixels[index + 3] = 255;
                
            }
        }
        /*Update diffImg pixels accordingly.*/
        diffImg.updatePixels();
    }
    
    prevImg = createImage(smallW,smallH);
    prevImg.copy(currImg, 0, 0, smallW, smallH, 0, 0, smallW, smallH);
    image(currImg, 840, 0);
    diffImg.filter(THRESHOLD,threshold);
    image(diffImg, 0, 0, camera.width, camera.height);
    grid.update(diffImg); /*Update the grid, check if any of the instrument sections need to be triggered.*/
    
    /*Color-cover - The transparent layer of colour covering the camera output to give the white part of the output a visible hint of colour*/
    colorMode(HSB); /*Set to HSB so that the shade can more easily modified by changing just one value (hue).*/
    fill(colorCoverSlider.value(),100,50,0.2);
    rect(0,0,camera.width,camera.height);
    fill(255);
    rect(640,0,smallW,smallH)
    noFill();
    colorMode(RGB);
    
    /*Chord text - Indicates to the user what chord has been triggered.*/
    stroke(255);
    fill(255);
    textSize(40);
    if(C == true)
    {
         text('C', camera.width/4 - (camera.width/4)/2, camera.height/2);
    }
    if(G == true)
    {
          text('G', camera.width/4 + (camera.width/4)/2, camera.height/2);
    }
    if(Am == true)
    {
          text('Am', camera.width/2 + (camera.width/4)/2, camera.height/2);
    }
    if(F == true)
    {
          text('F', camera.width - camera.width/4 + (camera.width/4)/2, camera.height/2);
    }
      
    /*As per regulations the audio context has to be triggered by user interaction, until the user presses a key and audioInit is set to true this message will be displayed. The user can still just move around and check how the camera output is affected to get a hang of how to play the instrument.*/
    if(!audioInit)
    {
         textAlign(CENTER);
         textSize(32);
         fill(255);
         text("Press any key to begin", width/2, height/2);
         return;
    }
                     
}

function mousePressed() 
{
    /*Set the threshold of the p5j.js THRESHOLD filter being used.*/
    threshold = map(mouseX,0,640,0,1);
}

/*Setup of grid being used to trigger different chords.*/
var Grid = function(_w, _h){
this.diffImg = 0;
this.worldWidth = _w;
this.worldHeight = _h;
this.noteWidth = this.worldWidth/4;
this.numOfNotesX = 4;
this.numOfNotesY = 4;
this.arrayLength = this.numOfNotesX * this.numOfNotesY;
this.noteStates = [];
this.noteStates =  new Array(this.arrayLength).fill(0);
this.colorArray = [];

/*Update the pixels, see if any section has been triggered. */
this.update = function(_img){
  this.diffImg = _img;
  this.diffImg.loadPixels();
  for (var x = 0; x < this.diffImg.width; x += 1) {
      for (var y = 0; y < this.diffImg.height; y += 1) {
          var index = (x + (y * this.diffImg.width)) * 4;
          var state = diffImg.pixels[index + 0];
          /*If any pixel is white within an area of the grid then activate that areas drum section.*/
          if (state==255){
            var screenX = map(x, 0, this.diffImg.width, 0, this.worldWidth);
            var screenY = map(y, 0, this.diffImg.height, 0, this.worldHeight);
            var noteIndexX = int(screenX/this.noteWidth);
            var noteIndexY = int(screenY/this.noteWidth);
            var noteIndex = noteIndexX + noteIndexY*this.numOfNotesX;
            this.noteStates[noteIndex] = 1;
          }
      }
  }

  /*Ages the sections so that they do not stay triggered forever*/
  for (var i=0; i<this.arrayLength;i++){
    this.noteStates[i]-= 0.05;
    this.noteStates[i]=constrain(this.noteStates[i],0,1);
  }

  this.draw();
    
};
    
    
this.draw = function(){
    
  push();
  noStroke();
     /*Loop through each section of grid and if they are active (value of this.noteStates is greater than 0), then draw the outline of the drum sections when they have been triggered, and setting their corresponding booleans to true so as to trigger the sound of it.*/
  for (var x=0; x<this.numOfNotesX; x++){
    for (var y=0; y<this.numOfNotesY; y++){
            var posX = this.noteWidth/2 + x*this.noteWidth;
            var posY = this.noteWidth/2 + y*this.noteWidth;
            var noteIndex = x + (y * this.numOfNotesX);
        
                stroke(255);
                noFill();
                
                /*C-chord indication*/
                if(this.noteStates[0]>0 || this.noteStates[4]>0 || this.noteStates[8]>0)
                {
                    C = true;

                    rect(1,1,this.worldWidth/4, this.worldHeight-1);
                }
                else
                {
                    C = false;
                }
                
                /*G-chord indication*/
                if(this.noteStates[1]>0 || this.noteStates[5]>0 || this.noteStates[9]>0)
                {
                    G = true;
                    rect(this.worldWidth - this.worldWidth/4 * 3, 1, this.worldWidth/4, this.worldHeight-1);  
                }
                else
                {
                    G = false;
                }
                
                /*Am-chord indication*/
                if(this.noteStates[2]>0 || this.noteStates[6]>0 || this.noteStates[10]>0)
                {
                    Am = true;
                    rect(this.worldWidth/2, 1, this.worldWidth/4, this.worldHeight-1);
                }
                else
                {
                    Am = false;
                }
                
                /*F-chord indication*/
                if(this.noteStates[3]>0 || this.noteStates[7]>0 || this.noteStates[11]>0)
                {
                    F = true;
                    rect(this.worldWidth - this.worldWidth/4, 1, this.worldWidth/4, this.worldHeight - 1);
                }
                else
                {
                    F = false;
                }

        }
  }
  pop();
}

};

/*Once a key has been pressed the audio context is active and the user can start playing the instrument.*/
function keyPressed()
{
    if(!audioInit)
    {
       audioContext.init();
       audioInit = true;
    }
    
}
